//
//  NSObject.swift
//  DesafioBRQ
//
//  Created by Ronilson on 22/03/2018.
//  Copyright © 2018 Ronilson Batista. All rights reserved.
//

import UIKit

protocol Identifying { }

extension Identifying where Self : NSObject {
    
    static var identifier: String { return String(describing: self) }
}

extension NSObject: Identifying { }
