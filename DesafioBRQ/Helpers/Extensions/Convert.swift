//
//  Convert.swift
//  DesafioBRQ
//
//  Created by Ronilson on 23/03/2018.
//  Copyright © 2018 Ronilson Batista. All rights reserved.
//

import Foundation

extension String {
    func toDouble() -> Double? {
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = Locale(identifier: "pt_BR")
        numberFormatter.numberStyle = .decimal
        return numberFormatter.number(from: self)?.doubleValue
    }
}

extension Double {
    func formatedAsCurreny() -> String {
        return String(format: "R$ %.2f", locale: Locale(identifier: "pt_BR"), self)
    }
    
    func toInt() -> Int? {
        if self > Double(Int.min) && self < Double(Int.max) {
            return Int(self)
        } else {
            return nil
        }
    }
}


