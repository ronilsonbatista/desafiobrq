//
//  CarDetailProtocol.swift
//  DesafioBRQ
//
//  Created by Ronilson on 24/03/2018.
//  Copyright © 2018 Ronilson Batista. All rights reserved.
//

import Foundation

protocol CarDetailProtocol : class {
    
    func startLoading()
    func stopLoading()
    func setup()
    func showAlertError(with title: String, message: String, buttonTitle: String)
}
