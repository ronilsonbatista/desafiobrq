//
//  CarDetailPresenter.swift
//  DesafioBRQ
//
//  Created by Ronilson on 24/03/2018.
//  Copyright © 2018 Ronilson Batista. All rights reserved.
//

import Foundation

import Foundation

final class CarDetailPresenter {
    
    fileprivate unowned let view: CarDetailProtocol
    fileprivate let service: CarService
    fileprivate(set) var cars: CarModel?
    
    init(view: CarDetailProtocol) {
        self.view = view
        self.service = CarService()
    }
}

// MARK: - Public methods

extension CarDetailPresenter {
    
    func loadCars(id: Int) {
        self.view.startLoading()
        self.service.getCarsDetail(id, success: { result  in
            
            if result.id == nil {
                self.requestError(errorDescription: "Nenhum Produto disponível no momento")
                return
            }
            
            self.cars = result
            print("Cars \(result)")
            self.view.setup()
            self.view.stopLoading()
        }) { error in
            self.requestError(errorDescription: error)
        }
    }
    
}

// MARK: - Private methods

extension CarDetailPresenter {
    
    fileprivate func requestError(errorDescription: String) {
        self.view.stopLoading()
        self.view.showAlertError(with: "Erro", message: errorDescription, buttonTitle: "OK")
    }
}
