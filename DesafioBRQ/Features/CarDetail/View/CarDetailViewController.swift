//
//  CarDetailViewController.swift
//  DesafioBRQ
//
//  Created by Ronilson on 24/03/2018.
//  Copyright © 2018 Ronilson Batista. All rights reserved.
//

import UIKit
import SVProgressHUD
import AlamofireImage
import RealmSwift

class CarDetailViewController: UITableViewController {
    
    @IBOutlet fileprivate weak var containerView: UIView!
    @IBOutlet fileprivate weak var containerShadowView: UIView!
    @IBOutlet fileprivate weak var containerButtonView: UIView!
    @IBOutlet fileprivate weak var containerShadowButtonView: UIView!
    @IBOutlet fileprivate weak var containerNumberView: UIView!
    @IBOutlet fileprivate weak var containerShadowNumberView: UIView!
    @IBOutlet fileprivate weak var txtNumberLabel: UILabel!
    @IBOutlet fileprivate weak var carName: UILabel!
    @IBOutlet fileprivate weak var carPrice: UILabel!
    @IBOutlet fileprivate weak var carDescription: UILabel!
    @IBOutlet fileprivate weak var carImage: UIImageView!
    
    fileprivate var presenter: CarDetailPresenter!
    fileprivate var price = Double()
    fileprivate var count : Int = 1
    fileprivate var countValue : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.presenter = CarDetailPresenter(view: self)
        self.presenter.loadCars(id: AppControl.shared.id)
        self.setCellAttributes()
        self.txtNumberLabel.text = "1"
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
}

// MARK: - TableViewDataSource

extension CarDetailViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if AppControl.shared.cartCalledMe == true {
            if indexPath.row == 0 { return 265 }
            else if indexPath.row == 1 { return 50 }
            else if indexPath.row == 2 { return 50 }
            else if indexPath.row == 3 { return 0 }
            else if indexPath.row == 4 {
                self.containerButtonView.isHidden = true
                self.containerShadowButtonView.isHidden = true
                return 280
            }
        } else {
            if indexPath.row == 0 { return 265 }
            else if indexPath.row == 1 { return 50 }
            else if indexPath.row == 2 { return 50 }
            else if indexPath.row == 3 { return 78 }
            else if indexPath.row == 4 {
                self.containerButtonView.isHidden = false
                self.containerShadowButtonView.isHidden = false
                return 290
            }
        }
        return 0
    }
}

// MARK: - Action methods

extension CarDetailViewController {
    
    @IBAction func increaseValue(_ sender: UIButton) {
        self.count = 1 + self.count
        self.countValue = "\(self.count)"
        self.txtNumberLabel.text = self.countValue
        self.multiply(value: count)
    }
    
    @IBAction func decreaseValue(_ sender: UIButton) {
        if count == 0 { print("Count zero") }
        else { count = count - 1 }
        countValue = "\(count)"
        self.txtNumberLabel.text = countValue
        self.multiply(value: count)
    }
    
    @IBAction func saveToCart(_ sender: UIButton) {
        self.addToCart()
    }
}

// MARK: - Private methods

extension CarDetailViewController {
    
    fileprivate func setCellAttributes() {
        ButtonViewParameterization.cornerRadius(view: self.containerButtonView)
        ButtonViewParameterization.shadowView(view: self.containerShadowButtonView, color: AppControl.shared.colorPrimary)
        ButtonViewParameterization.cornerRadius(view: self.containerView)
        ButtonViewParameterization.shadowView(view: self.containerShadowView, color: AppControl.shared.colorSnow)
        ButtonViewParameterization.cornerRadius(view: self.containerNumberView)
        ButtonViewParameterization.shadowView(view: self.containerShadowNumberView, color: UIColor.white)
    }
    
    fileprivate func multiply(value: Int) {
        if value == 0 { return }
        var x = String()
        x = "\((self.presenter.cars?.price)! * value)"
        self.price = x.toDouble()!
        print(self.price)
        self.carPrice.text =  price.formatedAsCurreny()
        
    }
    
    fileprivate func addToCart() {
        
        guard let car = self.presenter.cars else { return }
        
//        if self.txtNumberLabel.text == "0" {
//            self.txtNumberLabel.text = "1"
//        }
        
        let x : Int = Int(self.txtNumberLabel.text!)!
        if x > car.amount! {
            let value : String = String(car.amount!)
            Alert.show(delegate: self, title: "Quantidade disponível", message: "A quantidade disponível no estoque é de \(value) carros. ") { _ in
            }
        } else { self.saveDb() }
    }
    
    fileprivate func saveDb() {
        let realm = try! Realm()
        
        try! realm.write {
            let carDatabase = CarDatabase()
            guard let car = self.presenter.cars else { return }
            carDatabase.id = String(car.id!)
            carDatabase.name = car.name!
            carDatabase.carDescription = car.description!
            carDatabase.brand = car.brand!
            carDatabase.price = String(self.price.toInt()!)
            carDatabase.amount = self.txtNumberLabel.text!
            
            if let image = self.carImage.image {
                carDatabase.image = UIImagePNGRepresentation(image) as NSData?
            }
            realm.add(carDatabase)
        }
        
        Alert.show(delegate: self, title: "Carro adicionado", message: "O Carro foi com adicionado no carrinho") { _ in
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
}

// MARK: - ViewProtocol

extension CarDetailViewController: CarDetailProtocol {
    
    func startLoading() {
        SVProgressHUD.setDefaultStyle(.custom)
        SVProgressHUD.setForegroundColor(.white)
        SVProgressHUD.setBackgroundColor(UIColor.lightGray)
        SVProgressHUD.setDefaultMaskType(.clear)
        SVProgressHUD.show()
    }
    
    func stopLoading() { SVProgressHUD.dismiss()}
    
    func showAlertError(with title: String, message: String, buttonTitle: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func setup() {
        guard let car = self.presenter.cars else { return }
        
        self.carName.text = car.name
        self.carDescription.text = car.description
        self.price = String(car.price!).toDouble()!
        self.carPrice.text =  price.formatedAsCurreny()
        self.carPrice.textColor = AppControl.shared.colorPrimary
        
        if let imageString = self.presenter.cars?.image, let imageURL = URL(string: imageString) {
            self.carImage!.af_setImage(withURL: imageURL)
        }
    }
}
