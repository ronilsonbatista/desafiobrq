//
//  CartProtocol.swift
//  DesafioBRQ
//
//  Created by Ronilson on 24/03/2018.
//  Copyright © 2018 Ronilson Batista. All rights reserved.
//

import Foundation

protocol CartProtocol : class {
    
    func navigationItem()
    func totalPrice()
    func addRightBarButtonItems()
    func updateData()
    func cleanPurchases()
    func setAttributesView()
    
}
