//
//  CartPresenter.swift
//  DesafioBRQ
//
//  Created by Ronilson on 24/03/2018.
//  Copyright © 2018 Ronilson Batista. All rights reserved.
//

import Foundation

final class CartPresenter {
    
    fileprivate unowned let view: CartProtocol
    
    init(view: CartProtocol) {
        self.view = view
    }
}

// MARK: - Public methods

extension CartPresenter {
    
    func setupInitializerView() {
        self.view.navigationItem()
        self.view.updateData()
        self.view.addRightBarButtonItems()
        self.view.totalPrice()
        self.view.setAttributesView()
    }
    
}

