//
//  CartController.swift
//  DesafioBRQ
//
//  Created by Ronilson on 24/03/2018.
//  Copyright © 2018 Ronilson Batista. All rights reserved.
//

import UIKit
import RealmSwift

class CartController: UIViewController, UITableViewDelegate {
    
    @IBOutlet fileprivate weak var tableView: UITableView!
    @IBOutlet fileprivate weak var cardView: UIView!
    @IBOutlet fileprivate weak var valueView: UIView!
    @IBOutlet fileprivate weak var containerButtonView: UIView!
    @IBOutlet fileprivate weak var containerShadowButtonView: UIView!
    @IBOutlet fileprivate weak var containerView: UIView!
    @IBOutlet fileprivate weak var containerShadowView: UIView!
    @IBOutlet fileprivate weak var totalValue: UILabel!
    @IBOutlet fileprivate weak var totalValueError: UILabel!
    @IBOutlet fileprivate weak var totalValueErrorImage: UIImageView!
    @IBOutlet fileprivate weak var purchase: UIButton!
    
    fileprivate var dataBase: Realm?
    fileprivate var productDb: RealmSwift.Results<CarDatabase>?
    fileprivate let rowHeightEmpty: CGFloat = 500
    fileprivate let rowHeightBuy: CGFloat = 211
    fileprivate var presenter: CartPresenter!
    fileprivate var total = 0.0
    fileprivate var totalAmount = 0.0
    fileprivate var totalAmountValue = Int()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dataBase = try! Realm()
        self.presenter = CartPresenter(view: self)
        self.presenter.setupInitializerView()
        self.CartValue()
        
        self.totalValueErrorImage.isHidden = true
        self.totalValueError.isHidden = true
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: CartViewCell.identifier, bundle: nil), forCellReuseIdentifier: CartViewCell.identifier)
        self.tableView.register(UINib(nibName: WithoutViewCell.identifier, bundle: nil), forCellReuseIdentifier: WithoutViewCell.identifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.presenter.setupInitializerView()
        
        if  AppControl.shared.paymentSave == true {
            guard let dataBase = self.dataBase else { return }
            try! dataBase.write { dataBase.delete(productDb!) }
            self.updateData()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        tabBarItem = UITabBarItem(title: "cesto", image: UIImage(named: "shopping-icon"), tag: 1)
        self.tabBarController?.tabBar.backgroundColor = UIColor.white
        self.tabBarController?.tabBar.tintColor =  AppControl.shared.colorPrimary
        self.tabBarController?.tabBar.unselectedItemTintColor = UIColor.black
    }
}

// MARK: - Table view data source

extension CartController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerNoBuy = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 60))
        return headerNoBuy
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        guard let buy = self.productDb else { return 0 }
        if buy.count == 0 { return rowHeightEmpty }
        return rowHeightBuy
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let buy = self.productDb else { return 0 }
        if buy.count == 0 { return 1 }
        return buy.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let buy = self.productDb else { return UITableViewCell() }
        if buy.count == 0 {
            self.cardView.isHidden = true
            self.valueView.isHidden = true
            
            let cell = tableView.dequeueReusableCell(withIdentifier: WithoutViewCell.identifier, for: indexPath) as! WithoutViewCell
            cell.setCellAttributes(txtWithout: "Seu Carrinho Está Vazio", image: UIImage(named: "sad")!)
            return cell
        } else {
            self.cardView.isHidden = false
            self.valueView.isHidden = false
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CartViewCell.identifier, for: indexPath) as! CartViewCell
            cell.delegate = self
            cell.setContent(car: buy[indexPath.row])
            cell.didSelectCart.tag = indexPath.row
            cell.didSelectCart.addTarget(self, action: #selector(btnTapped(_:)), for: .touchUpInside)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor(white: 1, alpha: 0.5)
    }
}

// MARK: - UIStoryboardSegue

extension CartController {
    
    override internal func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            switch identifier {
            case "PurchaseController":
                if let paymentController = segue.destination as? PurchaseController {
                    paymentController.amount = self.total.formatedAsCurreny()
                    self.Amount()
                    paymentController.totalProducts = self.totalAmountValue
                }
            default: print("Identifier \(identifier) isn't a valid segue")
            }
        }
    }
}

// MARK: - PrivateMethods

extension CartController {
    
    private func CartValue() {
        
        self.totalValue.text = self.total.formatedAsCurreny()
        self.totalValue.textColor = AppControl.shared.colorPrimary
        
        if self.total > 100000.0 {
            self.totalValue.textColor = UIColor.red
            self.totalValueErrorImage.isHidden = false
            self.totalValueError.isHidden = false
            self.purchase.isEnabled = false
            self.purchase.setTitleColor(UIColor.gray, for: UIControlState.normal)
            ButtonViewParameterization.shadowView(view: self.containerShadowButtonView, color: AppControl.shared.colorSnow)
            
        } else {
            self.totalValue.textColor = AppControl.shared.colorPrimary
            self.totalValueErrorImage.isHidden = true
            self.totalValueError.isHidden = true
            self.purchase.isEnabled = true
            self.purchase.setTitleColor(UIColor.white, for: UIControlState.normal)
            ButtonViewParameterization.shadowView(view: self.containerShadowButtonView, color: AppControl.shared.colorPrimary)
        }
    }
}

extension CartController: CartDelegate {
    
    func didSelectDetails(id: Int) {
        AppControl.shared.id = id
        AppControl.shared.cartCalledMe = true
        performSegue(withIdentifier: "details", sender: self)
    }
}

// MARK: - ViewProtocol

extension CartController: CartProtocol {
    
    func setAttributesView() {
        ButtonViewParameterization.cornerRadius(view: self.containerButtonView)
        
        ButtonViewParameterization.cornerRadius(view: self.containerView)
        ButtonViewParameterization.shadowView(view: self.containerShadowView, color: UIColor.white)
    }
    
    @objc func btnTapped(_ sender : UIButton) {
        guard let purchases = self.productDb, let dataBase = self.dataBase else {
            return
        }
        try! dataBase.write {
            dataBase.delete(purchases[sender.tag])
        }
        updateData()
    }
    
    func totalPrice() {
        self.total = 0.0
        
        if productDb?.count == 0 {
            return
        }
        if let orders = productDb {
            for order in orders  {
                let price = order.price.toDouble()
                total += (price)!
            }
        }
        print("total: \(total)")
    }
    
    func Amount() {
        self.totalAmount = 0.0
        
        if productDb?.count == 0 {
            return
        }
        if let orders = productDb {
            for order in orders  {
                let amount = order.amount.toDouble()
                totalAmount += (amount)!
            }
        }
        self.totalAmountValue = Int(totalAmount)
        print("totalAmount: \(totalAmount)")
    }
    
    func navigationItem() {
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white, NSAttributedStringKey.font : UIFont.systemFont(ofSize: 18.0, weight: UIFont.Weight.semibold)]
        self.navigationController?.navigationBar.barTintColor = AppControl.shared.colorPrimary
    }
    
    func addRightBarButtonItems() {
        let cleanItem = UIBarButtonItem(title: "Deletar", style: .plain, target: self, action: #selector(self.cleanPurchases))
        cleanItem.tintColor = .white
        self.navigationItem.setRightBarButtonItems([cleanItem], animated: true)
    }
    
    func updateData() {
        guard let dataBase = self.dataBase else { return }
        productDb = dataBase.objects(CarDatabase.self)
        self.totalPrice()
        self.CartValue()
        tableView.reloadData()
    }
    
    @objc func cleanPurchases() {
        if productDb?.count == 0 {
            Alert.show(delegate: self, title: ";)", message: "Nenhuma compra realizada", buttonTitle: "OK") { _ in }
        }else {
            Alert.show(delegate: self, title: "Deletar todas os produtos?", message: "Você tem certeza que deseja apagar todas os produtos.", buttonTitle: "OK", hasChoice: true) { choice in
                
                if choice {
                    guard let dataBase = self.dataBase else { return }
                    try! dataBase.write { dataBase.delete(self.productDb!) }
                    self.updateData()
                }
            }
        }
    }  
}
