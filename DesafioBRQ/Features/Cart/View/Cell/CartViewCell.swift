//
//  CartViewCell.swift
//  DesafioBRQ
//
//  Created by Ronilson on 25/03/2018.
//  Copyright © 2018 Ronilson Batista. All rights reserved.
//

import UIKit

protocol CartDelegate {
    func didSelectDetails(id: Int)
}

class CartViewCell: UITableViewCell {
    
    @IBOutlet fileprivate weak var containerDetailsView: UIView!
    @IBOutlet fileprivate weak var containerShadowDetailsView: UIView!
    @IBOutlet fileprivate weak var containerRemoveView: UIView!
    @IBOutlet fileprivate weak var containerShadowRemoveView: UIView!
    @IBOutlet fileprivate weak var containerAmountView: UIView!
    @IBOutlet fileprivate weak var containerShadowAmountView: UIView!
    @IBOutlet fileprivate weak var carName: UILabel!
    @IBOutlet fileprivate weak var carPrice: UILabel!
    @IBOutlet fileprivate weak var carAmount: UILabel!
    @IBOutlet fileprivate weak var productImages: UIImageView!
    @IBOutlet weak var didSelectCart: UIButton!
    
    fileprivate var price: Double?
    var cars: CarDatabase?
    var delegate: CartDelegate?
    var index = IndexPath()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setCellAttributes()
    }
}

// MARK: - Private methods
extension CartViewCell {
    fileprivate func setCellAttributes() {
        ButtonViewParameterization.cornerRadius(view: self.containerDetailsView)
        ButtonViewParameterization.shadowView(view: self.containerShadowDetailsView, color: AppControl.shared.colorPrimary)
        ButtonViewParameterization.cornerRadius(view: self.containerRemoveView)
        ButtonViewParameterization.shadowView(view: self.containerShadowRemoveView, color: UIColor.red)
        ButtonViewParameterization.cornerRadius(view: self.containerAmountView)
        ButtonViewParameterization.shadowView(view: self.containerShadowAmountView, color: UIColor.white)
    }
}

// MARK: - Public methods
extension CartViewCell {
    func setContent(car: CarDatabase) {
        self.cars = car
        self.carName.text = car.name
        self.price = car.price.toDouble()
        self.carPrice.text = price!.formatedAsCurreny()
        self.carAmount.text = car.amount
        
        if let data = car.image as? Data {
            self.productImages.image = UIImage(data: data)
        }
    }
}

// MARK: - Action methods
extension CartViewCell {
    
    @IBAction func didSelectDetails(_ sender: UIButton) {
        guard let carModel = self.cars else {
            return
        }
        self.delegate?.didSelectDetails(id: Int(carModel.id)!)
    }
}
