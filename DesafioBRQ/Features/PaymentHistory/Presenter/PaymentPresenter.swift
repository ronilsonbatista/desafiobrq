//
//  PaymentPresenter.swift
//  DesafioBRQ
//
//  Created by Ronilson on 26/03/2018.
//  Copyright © 2018 Ronilson Batista. All rights reserved.
//

import Foundation

final class PaymentHistoryPresenter {
    
    fileprivate unowned let view: PaymentHistoryProtocol
    
    init(view: PaymentHistoryProtocol) {
        self.view = view
    }
}

// MARK: - Public methods

extension PaymentHistoryPresenter {
    
    func setupInitializerView() {
        self.view.updateData()
        
    }
}
