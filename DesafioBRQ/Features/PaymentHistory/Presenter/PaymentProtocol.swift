//
//  PaymentProtocol.swift
//  DesafioBRQ
//
//  Created by Ronilson on 26/03/2018.
//  Copyright © 2018 Ronilson Batista. All rights reserved.
//

import Foundation

protocol PaymentHistoryProtocol : class {
    
    func updateData()
}
