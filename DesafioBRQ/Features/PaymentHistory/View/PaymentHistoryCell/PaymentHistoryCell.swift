//
//  PaymentHistoryCell.swift
//  DesafioBRQ
//
//  Created by Ronilson on 26/03/2018.
//  Copyright © 2018 Ronilson Batista. All rights reserved.
//

import UIKit

class PaymentHistoryCell: UITableViewCell {
    
    @IBOutlet fileprivate weak var productCardName: UILabel!
    @IBOutlet fileprivate weak var productPrice: UILabel!
    @IBOutlet fileprivate weak var productDate: UILabel!
    @IBOutlet fileprivate weak var productTotal: UILabel!
    
    var color: UIColor = AppControl.shared.colorPrimary
    
    func setContent(savePayment: SavePurchase) {
        
        self.productCardName.text = savePayment.productCardName
        self.productCardName.textColor = self.color
        self.productPrice.text = savePayment.productPrice
        self.productPrice.textColor = self.color
        self.productDate.text = savePayment.productDate
        self.productDate.textColor = self.color
        self.productTotal.text = savePayment.productsTotal
        self.productTotal.textColor = self.color
    }
    
}
