//
//  PaymentHistoryController.swift
//  DesafioBRQ
//
//  Created by Ronilson on 26/03/2018.
//  Copyright © 2018 Ronilson Batista. All rights reserved.
//

import UIKit
import RealmSwift

class PaymentHistoryController: UITableViewController {
    
    fileprivate var presenter: PaymentHistoryPresenter!
    fileprivate var dataBase: Realm?
    fileprivate var productDb: RealmSwift.Results<SavePurchase>?
    fileprivate let rowHeightEmpty: CGFloat = 316
    fileprivate let rowHeightBuy: CGFloat = 170
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataBase = try! Realm()
        
        self.presenter = PaymentHistoryPresenter(view: self)
        self.presenter.setupInitializerView()
        
        self.navigationItem.title = "Histórico"
        let backButton = UIBarButtonItem(title: "Cancelar", style: UIBarButtonItemStyle.plain, target: self, action: #selector(goBack))
        self.navigationController?.navigationBar.tintColor = UIColor.white
        navigationItem.leftBarButtonItem = backButton
        
        self.tableView.register(UINib(nibName: PaymentHistoryCell.identifier, bundle: nil), forCellReuseIdentifier: PaymentHistoryCell.identifier)
        self.tableView.register(UINib(nibName: WithoutViewCell.identifier, bundle: nil), forCellReuseIdentifier: WithoutViewCell.identifier)
    }
}

// MARK: - Table view data source

extension PaymentHistoryController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    @objc fileprivate func goBack(){ self.navigationController?.popViewController(animated: false) }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        guard let buy = self.productDb else { return 0 }
        if buy.count == 0 { return rowHeightEmpty }
        return rowHeightBuy
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let buy = self.productDb else { return 0 }
        if buy.count == 0 { return 1 }
        return buy.count
    }
    
    internal override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let buy = self.productDb else { return UITableViewCell() }
        if buy.count == 0 {
        let cell = tableView.dequeueReusableCell(withIdentifier: WithoutViewCell.identifier, for: indexPath) as! WithoutViewCell
          cell.setCellAttributes(txtWithout: "Nenhuma compra foi realizada.", image: UIImage(named: "nobuy")!)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: PaymentHistoryCell.identifier, for: indexPath) as! PaymentHistoryCell
            cell.setContent(savePayment: buy[indexPath.row])
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor(white: 1, alpha: 0.5)
    }
}

// MARK: - ViewProtocol

extension PaymentHistoryController: PaymentHistoryProtocol {
    
    func updateData() {
        guard let dataBase = self.dataBase else { return }
        productDb = dataBase.objects(SavePurchase.self)
        tableView.reloadData()
    }
}

