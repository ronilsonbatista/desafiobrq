//
//  PurchaseController.swift
//  DesafioBRQ
//
//  Created by Ronilson on 25/03/2018.
//  Copyright © 2018 Ronilson Batista. All rights reserved.
//

import UIKit
import RealmSwift

class PurchaseController: UITableViewController {
    
    @IBOutlet fileprivate weak var containerButtonView: UIView!
    @IBOutlet fileprivate weak var containerShadowButtonView: UIView!
    @IBOutlet fileprivate weak var totalProductsLabel: UILabel!
    @IBOutlet fileprivate weak var amountLabel: UILabel!
    @IBOutlet fileprivate weak var nameCell: UITableViewCell!
    @IBOutlet fileprivate weak var cardNumberCell: UITableViewCell!
    @IBOutlet fileprivate weak var cvvCell: UITableViewCell!
    @IBOutlet fileprivate weak var nameTextField: UITextField!
    @IBOutlet fileprivate weak var cvvTextField: VSTextField!
    @IBOutlet fileprivate weak var cardNumberTextField: VSTextField!
    @IBOutlet fileprivate weak var cardValidationTextField: VSTextField!
    
    fileprivate var presenter: PurchasePresenter!
    fileprivate var datePayment = String()
    
    var amount = String()
    var totalProducts = Int()
    var currentNSDate = Date()
    var dateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.presenter = PurchasePresenter(view: self)
        self.presenter.setupInitializerView()
    }
}

// MARK: - Table view data source

extension PurchaseController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 8
    }
}

// MARK: - Action

extension PurchaseController {
    
    @IBAction func payment(_ sender: AnyObject) {
        
        if checkFields() {
            
            self.savePayment()
            Alert.show(delegate: self, title: "Compra Finalizada", message: "Sua compra foi finalizada com sucesso") { _ in
                AppControl.shared.paymentSave = true
                self.navigationController?.popToRootViewController(animated: true)
            }
            self.resetField()
        } else {
            Alert.show(delegate: self, title: "Preenchar todos os campos", message: "Para efetuar uma compra você precisa preencher todos os campos", buttonTitle: "Tente novamente") { _ in }
        }
    }
}

// MARK: - ViewProtocol

extension PurchaseController:PurchaseProtocol {
    
    func savePayment() {
        
        let realm = try! Realm()
        
        try! realm.write {
            
            let saveDb = SavePurchase()
            self.datePayment = formatDate()
            saveDb.productCardName = self.nameTextField.text!
            saveDb.productPrice = self.amount
            saveDb.productsTotal = String(self.totalProducts)
            saveDb.productDate = self.datePayment
            realm.add(saveDb)
            
        }
    }
    
    func addDoneButton() {
        let keyboardToolbar = UIToolbar(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 0, height: 44)))
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        let doneBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "closeDown"), style: UIBarButtonItemStyle.done, target: view, action: #selector(UIView.endEditing(_:)))
        
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        
        cardNumberTextField.inputAccessoryView = keyboardToolbar
        cvvTextField.inputAccessoryView = keyboardToolbar
        cardValidationTextField.inputAccessoryView = keyboardToolbar
    }
    
    func resetField() {
        nameTextField.text = ""
        cardNumberTextField.text = ""
        cardValidationTextField.text = ""
        cvvTextField.text = ""
    }
    
    func checkFields() -> Bool {
        
        self.tintCell()
        
        guard let name = self.nameTextField.text, let number = self.cardNumberTextField.text, let cvv = self.cvvTextField.text, let year = self.cardValidationTextField.text else {
            return false
        }
        
        if name.count >= 2, number.count == 16, year.count == 4, cvv.count >= 3 { return true }
        else {return false }
    }
    
    func tintCell() {
        
        guard let name = self.nameTextField.text, let number = self.cardNumberTextField.text, let cvv = self.cvvTextField.text, let year = self.cardValidationTextField.text else {
            return
        }
        
        let errorColor = AppControl.shared.colorError
        
        if name.count < 2 { nameCell.backgroundColor = errorColor }
        else { nameCell.backgroundColor = .clear }
        if number.count < 16 { cardNumberCell.backgroundColor = errorColor }
        else { cardNumberCell.backgroundColor = .clear }
        if year.count < 4 { cvvCell.backgroundColor = errorColor }
        else if cvv.count < 3 { cvvCell.backgroundColor = errorColor }
        else { cvvCell.backgroundColor = .clear }
    }
    
    func formatDate() -> String {
        self.currentNSDate = Date()
        self.datePayment = String(describing: self.currentNSDate)
        self.dateFormatter.dateStyle = .medium
        self.dateFormatter.timeStyle = .medium
        
        self.dateFormatter.locale = Locale(identifier: "pt_BR")
        let formattedDate = self.dateFormatter.string(from: self.currentNSDate)
        
        return formattedDate
    }
    
    func setAttributesView() {
        ButtonViewParameterization.cornerRadius(view: self.containerButtonView)
        ButtonViewParameterization.shadowView(view: self.containerShadowButtonView, color: AppControl.shared.colorPrimary)
    }
    
    func setupView() {
        self.amountLabel.text = self.amount
        self.amountLabel.textColor = AppControl.shared.colorPrimary
        self.totalProductsLabel.text = String(self.totalProducts)
        self.totalProductsLabel.textColor = AppControl.shared.colorPrimary
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    func VSTextField() {
        self.cardNumberTextField.setFormatting("#### #### #### ####", replacementChar: "#")
        self.cardValidationTextField.setFormatting("##/##", replacementChar: "#")
        self.cvvTextField.setFormatting("###", replacementChar: "#")
    }
}
