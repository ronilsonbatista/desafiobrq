//
//  PurchaseProtocol.swift
//  DesafioBRQ
//
//  Created by Ronilson on 25/03/2018.
//  Copyright © 2018 Ronilson Batista. All rights reserved.
//

import Foundation

protocol PurchaseProtocol: class {
    
    func setupView()
    func setAttributesView()
    func VSTextField()
    func addDoneButton()
    func resetField()
    func checkFields() -> Bool
    func tintCell()
    func savePayment()
    func formatDate() -> String
}
