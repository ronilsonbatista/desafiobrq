//
//  PurchasePresenter.swift
//  DesafioBRQ
//
//  Created by Ronilson on 25/03/2018.
//  Copyright © 2018 Ronilson Batista. All rights reserved.
//

import Foundation

final class PurchasePresenter {
    
    fileprivate unowned let view: PurchaseProtocol
    
    init(view: PurchaseProtocol) {
        self.view = view
    }
}

// MARK: - Public methods

extension PurchasePresenter {
    
    func setupInitializerView() {
        self.view.setupView()
        self.view.setAttributesView()
        self.view.VSTextField()
        self.view.addDoneButton()
    }
}
