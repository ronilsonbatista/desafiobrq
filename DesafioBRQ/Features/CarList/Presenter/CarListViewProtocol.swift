//
//  CarListViewProtocol.swift
//  DesafioBRQ
//
//  Created by Ronilson on 22/03/2018.
//  Copyright © 2018 Ronilson Batista. All rights reserved.
//

import Foundation

protocol CarListViewProtocol : class {
    
    func navigationItem()
    func tabBarItem()
    func startLoading()
    func stopLoading()
    func reloadTableView()
    func showAlertError(with title: String, message: String, buttonTitle: String)
}

