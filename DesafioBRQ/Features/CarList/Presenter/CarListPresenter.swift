//
//  CarListPresenter.swift
//  DesafioBRQ
//
//  Created by Ronilson on 22/03/2018.
//  Copyright © 2018 Ronilson Batista. All rights reserved.
//

import Foundation

final class CarListPresenter {
    
    fileprivate unowned let view: CarListViewProtocol
    fileprivate let service: CarService
    fileprivate(set) var allCars: [CarModel] = []
    
    init(view: CarListViewProtocol) {
        self.view = view
        self.service = CarService()
    }
}

// MARK: - Public methods

extension CarListPresenter {
    
    func loadCars() {
        self.view.startLoading()
        self.service.getCars(success: { result  in
            
            if result.count == 0 {
                self.requestError(errorDescription: "Nenhum Produto disponível no momento")
                return
            }
            
            self.allCars = result
            print("allCars: \(result)")
            print("allCars: \(self.allCars.count)")
            self.view.reloadTableView()
            self.view.stopLoading()
            
        }) { error in
            self.requestError(errorDescription: error)
        }
    }
    
    func setupInitializerView() {
        self.view.navigationItem()
        self.view.tabBarItem()
    }
}

// MARK: - Private methods

extension CarListPresenter {
    
    fileprivate func requestError(errorDescription: String) {
        self.view.stopLoading()
        self.view.showAlertError(with: "Erro", message: errorDescription, buttonTitle: "OK")
    }
}
