//
//  CarItemViewCell.swift
//  DesafioBRQ
//
//  Created by Ronilson on 23/03/2018.
//  Copyright © 2018 Ronilson Batista. All rights reserved.
//

import UIKit
import AlamofireImage

protocol DidSelectDelegate {
    func didSelectDetails(id: Int)
    func didSelectCart(id: Int, name: String, description: String, brand: String, price: Int, amount: Int, images: UIImageView)
}

class CarItemViewCell: UITableViewCell {
    
    @IBOutlet fileprivate weak var containerDetailsView: UIView!
    @IBOutlet fileprivate weak var containerShadowDetailsView: UIView!
    @IBOutlet fileprivate weak var containerPurchaseView: UIView!
    @IBOutlet fileprivate weak var containerShadowPurchaseView: UIView!
    @IBOutlet fileprivate weak var carName: UILabel!
    @IBOutlet fileprivate weak var carPrice: UILabel!
    @IBOutlet fileprivate weak var carImage: UIImageView!
    
    fileprivate var price: Double?
    var cars: CarModel?
    var delegate: DidSelectDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setCellAttributes()
    }
}

// MARK: - Private methods
extension CarItemViewCell {
    fileprivate func setCellAttributes() {
        ButtonViewParameterization.cornerRadius(view: self.containerDetailsView)
        ButtonViewParameterization.shadowView(view: self.containerShadowDetailsView, color: AppControl.shared.colorPrimary)
        ButtonViewParameterization.cornerRadius(view: self.containerPurchaseView)
        ButtonViewParameterization.shadowView(view: self.containerShadowPurchaseView, color: AppControl.shared.colorPrimary)
    }
}

// MARK: - Public methods
extension CarItemViewCell {
    func setup(car: CarModel) {
        self.cars = car
        self.price = String(car.price!).toDouble()
        self.carPrice.text = price!.formatedAsCurreny()
        self.carName.text = car.name
        
        if let imageString = car.image, let imageURL = URL(string: imageString) {
            carImage!.af_setImage(withURL: imageURL)
        }
    }
}

// MARK: - Action methods
extension CarItemViewCell {
    
    @IBAction func didSelectDetails(_ sender: UIButton) {
        guard let carModel = self.cars else {
            return
        }
        self.delegate?.didSelectDetails(id: carModel.id!)
    }
    
    @IBAction func didSelectCart(_ sender: UIButton) {
        guard let carModel = self.cars else {
            return
        }
        self.delegate?.didSelectCart(id: carModel.id!, name: carModel.name!, description: carModel.description!, brand: carModel.brand!, price: carModel.price!, amount: carModel.amount!, images: self.carImage)
    }
}
