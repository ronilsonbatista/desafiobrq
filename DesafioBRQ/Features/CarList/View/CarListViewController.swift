//
//  CarListViewController.swift
//  DesafioBRQ
//
//  Created by Ronilson on 22/03/2018.
//  Copyright © 2018 Ronilson Batista. All rights reserved.
//

import UIKit
import SVProgressHUD
import RealmSwift

class CarListViewController: UITableViewController {
    
    fileprivate var presenter: CarListPresenter!
    fileprivate var index = IndexPath()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AppControl.shared.id = 0
        self.presenter = CarListPresenter(view: self)
        self.presenter.loadCars()
        self.presenter.setupInitializerView()
        self.tableView.register(UINib(nibName: CarItemViewCell.identifier, bundle: nil), forCellReuseIdentifier: CarItemViewCell.identifier)
    }
}

// MARK: - UITableViewDataSource

extension CarListViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.presenter.allCars.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : CarItemViewCell = tableView.dequeueReusableCell(withIdentifier: "CarItemViewCell", for: indexPath) as! CarItemViewCell
        self.index = indexPath
        cell.delegate = self
        cell.setup(car: self.presenter.allCars[indexPath.section])
        return cell
    }
}

// MARK: - DidSelectDelegate

extension CarListViewController: DidSelectDelegate {
    
    func didSelectCart(id: Int, name: String, description: String, brand: String, price: Int, amount: Int, images: UIImageView) {
        
        let realm = try! Realm()
        
        try! realm.write {
            let carDatabase = CarDatabase()
            carDatabase.id = String(id)
            carDatabase.name = name
            carDatabase.carDescription = description
            carDatabase.brand = brand
            carDatabase.price = String(price)
            carDatabase.amount = String("1")
            
            if let image = images.image {
                carDatabase.image = UIImagePNGRepresentation(image) as NSData?
            }
            realm.add(carDatabase)
        }
        
        Alert.show(delegate: self, title: "Carro adicionado", message: "O Carro foi com adicionado no carrinho") { _ in
        }
    }
    
    func didSelectDetails(id: Int) {
        AppControl.shared.id = id
        AppControl.shared.cartCalledMe = false
        performSegue(withIdentifier: "details", sender: self)
    }
}

// MARK: - ViewProtocol

extension CarListViewController: CarListViewProtocol {
    
    func navigationItem() {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white, NSAttributedStringKey.font : UIFont.systemFont(ofSize: 18.0, weight: UIFont.Weight.semibold)]
        self.navigationController?.navigationBar.barTintColor = AppControl.shared.colorPrimary
    }
    
    func tabBarItem() {
        tabBarItem = UITabBarItem(title: "destaques", image: UIImage(named: "star"), tag: 0)
        self.tabBarController?.tabBarItem.title = "destaques"
        self.tabBarController?.tabBar.backgroundColor = UIColor.white
        self.tabBarController?.tabBar.tintColor = AppControl.shared.colorPrimary
        self.tabBarController?.tabBar.unselectedItemTintColor = UIColor.black
    }
    
    func startLoading() {
        SVProgressHUD.setDefaultStyle(.custom)
        SVProgressHUD.setForegroundColor(.white)
        SVProgressHUD.setBackgroundColor(UIColor.lightGray)
        SVProgressHUD.setDefaultMaskType(.clear)
        SVProgressHUD.show()
    }
    
    func stopLoading() {
        SVProgressHUD.dismiss()
    }
    
    func reloadTableView() {
        UIView.transition(with: tableView!, duration: 0.35, options: .transitionCrossDissolve, animations: {
            self.tableView?.reloadData()
        })
    }
    
    func showAlertError(with title: String, message: String, buttonTitle: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
