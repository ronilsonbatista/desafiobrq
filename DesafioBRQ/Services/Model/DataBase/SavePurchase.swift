//
//  SavePurchase.swift
//  DesafioBRQ
//
//  Created by Ronilson on 25/03/2018.
//  Copyright © 2018 Ronilson Batista. All rights reserved.
//

import UIKit
import RealmSwift

class SavePurchase: Object {
    
    @objc dynamic var productCardName = ""
    @objc dynamic var productPrice = ""
    @objc dynamic var productDate = ""
    @objc dynamic var productsTotal = ""
    
}
