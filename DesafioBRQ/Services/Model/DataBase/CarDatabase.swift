//
//  CarDatabase.swift
//  DesafioBRQ
//
//  Created by Ronilson on 24/03/2018.
//  Copyright © 2018 Ronilson Batista. All rights reserved.
//

import UIKit
import RealmSwift

class CarDatabase: Object {
    
    @objc dynamic var id = ""
    @objc dynamic var name = ""
    @objc dynamic var carDescription = ""
    @objc dynamic var brand = ""
    @objc dynamic var price = ""
    @objc dynamic var amount = ""
    @objc dynamic var image: NSData? = nil
}
