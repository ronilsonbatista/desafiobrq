//
//  Car.swift
//  DesafioBRQ
//
//  Created by Ronilson on 22/03/2018.
//  Copyright © 2018 Ronilson Batista. All rights reserved.
//

import Foundation

struct CarModel : Codable {
    
    let id : Int?
    let name : String?
    let description : String?
    let brand : String?
    let amount : Int?
    let price : Int?
    let image : String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name = "nome"
        case description = "descricao"
        case brand = "marca"
        case amount = "quantidade"
        case price = "preco"
        case image = "imagem"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        brand = try values.decodeIfPresent(String.self, forKey: .brand)
        amount = try values.decodeIfPresent(Int.self, forKey: .amount)
        price = try values.decodeIfPresent(Int.self, forKey: .price)
        image = try values.decodeIfPresent(String.self, forKey: .image)
    }
    
}
