//
//  CarService.swift
//  DesafioBRQ
//
//  Created by Ronilson on 22/03/2018.
//  Copyright © 2018 Ronilson Batista. All rights reserved.
//

import Foundation

final class CarService {
    
    func getCars(success: @escaping (_ products: [CarModel]) -> Void, fail: @escaping (_ error: String) -> Void) {
        
        ServiceManager.shared.GetData(url: ServiceURL.car.value, parameters: nil, success: { result in
            let response = try! JSONDecoder().decode([CarModel].self, from: result)
            success(response)
        }, failure: { error in
            fail(error.description)
        })
    }
    
    func getCarsDetail(_ id: Int, success: @escaping (_ products: CarModel) -> Void, fail: @escaping (_ error: String) -> Void) {
        
        ServiceManager.shared.GetData(url: "\(ServiceURL.car.value)\(id)", parameters: nil, success: { result in
            let response = try! JSONDecoder().decode(CarModel.self, from: result)
            success(response)
        }, failure: { error in
            fail(error.description)
        })
    }
}
