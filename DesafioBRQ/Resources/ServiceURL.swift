//
//  ServiceURL.swift
//  DesafioBRQ
//
//  Created by Ronilson on 22/03/2018.
//  Copyright © 2018 Ronilson Batista. All rights reserved.
//

import Foundation

enum ServiceURL {
    case car
    
    var value: String {
        switch self {
        case .car: return "http://desafiobrq.herokuapp.com/v1/carro/"
        }
    }
}
