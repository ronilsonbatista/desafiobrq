//
//  AppControl.swift
//  DesafioBRQ
//
//  Created by Ronilson on 24/03/2018.
//  Copyright © 2018 Ronilson Batista. All rights reserved.
//

import UIKit

class AppControl {
    static public let shared = AppControl()
    
    let colorPrimary = UIColor(hexadecimal: 0x2F5D4D)
    let colorSnow = UIColor(hexadecimal: 0xF7F7F7)
    let colorError = UIColor.red
    var id = Int()
    var paymentSave: Bool = false
    var cartCalledMe: Bool = false
}
